import { Component, OnInit } from '@angular/core';
import { registrarUsuarioService } from '../../registrarUsuario.service';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-registrarUsuario',
  templateUrl: './registrarUsuario.component.html',
  styleUrls: ['../../../../../assets/css/style.css']
})
export class RegistrarUsuarioComponent implements OnInit {
  public usuarioRegistro = {
    usuario: '',
    correo: '',
    dni: '' ,
    numeroCelular : '',
    clave: '',
  };
  nombreCompleto: string;
  apellidoCompleto: string;
  password: string;
  Confpassword: string;
  mensaje: string;

  // tslint:disable-next-line: no-shadowed-variable
  constructor(public registrarUsuarioService: registrarUsuarioService) { }

  ngOnInit() {
    this.nombreCompleto = '';
    this.apellidoCompleto = '';
    this.password = '';
    this.Confpassword = '';
  }


  registrarUsuario(){
    if (this.validarCampos){
      this.usuarioRegistro.usuario = this.nombreCompleto + ' ' + this.apellidoCompleto;
      this.usuarioRegistro.clave = this.password;
      this.registrarUsuarioService.registrarUsuario(this.usuarioRegistro).subscribe(res => {
      document.getElementById('exampleModal').click();
      console.log(res);
    },
    error => {
      console.log('ERROR', error);
    });
    }else{
      document.getElementById('exampleModalError').click();
    }
  }

  validarCampos(): boolean{
    let salida = false;
    // tslint:disable-next-line: triple-equals
    if (this.nombreCompleto == ''){
      this.mensaje = 'INGRESE EL NOMBRE';
      salida = false;
    }
    // tslint:disable-next-line: triple-equals
    if (this.apellidoCompleto == ''){
      this.mensaje = 'INGRESE EL APELLIDO';
      salida = false;
    }
    // tslint:disable-next-line: triple-equals
    if (this.password == ''){
      this.mensaje = 'INGRESE LA CONTRASEÑA';
      salida = false;
    }
    // tslint:disable-next-line: triple-equals
    if (this.Confpassword == ''){
      this.mensaje = 'INGRESE LA CONFIRMACIÓN DE CONTRASEÑA';
      salida = false;
    }
    return salida;
  }

}
