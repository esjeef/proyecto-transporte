import { RegistrarUsuarioComponent } from './containers/registrarUsuario/registrarUsuario.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../core/shared/shared.module';
import { RegistrarUsuarioRoutingModule } from './registrarUsuario-routing.module';


@NgModule({
  imports: [
    SharedModule,
    RegistrarUsuarioRoutingModule
  ],
  declarations: [
    RegistrarUsuarioComponent
  ],
  exports: [],
  providers: []
})

export class RegistrarUsuarioModule {
  constructor() {}
}
