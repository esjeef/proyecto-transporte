import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicComponent } from './public.component';

const routes: Routes = [
  { path: '', component: PublicComponent, children: [
    { path: '', redirectTo: 'login', pathMatch: 'full'},
    { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },
    { path: 'registrarUsuario', loadChildren: () => import('./registrarUsuario/registrarUsuario.module')
    .then(m => m.RegistrarUsuarioModule) },
    { path: 'recuperarClave', loadChildren: () => import('./recuperarClave/recuperarClave.module')
    .then(m => m.RecuperarClaveModule) },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
