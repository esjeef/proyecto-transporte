import { Component, OnInit } from '@angular/core';
import { loginService } from '../../login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../../../../../assets/css/style.css']
})
export class LoginComponent implements OnInit {
  public usuario = {
    usuario: '',
    clave: '',
  };
  // tslint:disable-next-line: no-shadowed-variable
  constructor(private router: Router, public loginService: loginService) { }

  ngOnInit() {}

  login(){
    this.loginService.login(this.usuario).subscribe(res => {
      localStorage.setItem('identificador', res.identificador);
      this.router.navigate(['/admin/home']);
      console.log(res);
    },
    error => {
      console.log('ERROR', error);
    });
  }

  invitado(){
    localStorage.setItem('identificador', '1');
    this.router.navigate(['/admin/home']);
  }
}
