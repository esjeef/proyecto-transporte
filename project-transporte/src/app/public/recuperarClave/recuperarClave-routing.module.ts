import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecuperarClaveComponent } from './containers/recuperarClave/recuperarClave.component';

const routes: Routes = [
  { path: '', component: RecuperarClaveComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecuperarClaveRoutingModule {}
