import { NgModule } from '@angular/core';
import { SharedModule } from '../../core/shared/shared.module';
import { RecuperarClaveRoutingModule } from './recuperarClave-routing.module';
import { RecuperarClaveComponent } from './containers/recuperarClave/recuperarClave.component';


@NgModule({
  imports: [
    SharedModule,
    RecuperarClaveRoutingModule
  ],
  declarations: [
    RecuperarClaveComponent
  ],
  exports: [],
  providers: []
})

export class RecuperarClaveModule {
  constructor() {}
}
