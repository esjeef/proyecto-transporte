import { recuperarClaveService } from './../../recuperarClave.service';
import { Component, OnInit } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-recuperarClave',
  templateUrl: './recuperarClave.component.html',
  styleUrls: ['../../../../../assets/css/style.css']
})
export class RecuperarClaveComponent implements OnInit {
  public correoUsuario = {
    correo: ''
  };
  constructor(public recuperarService: recuperarClaveService) { }

  ngOnInit() {
  }

  enviarCorreo(){
    this.recuperarService.enviarCorreo(this.correoUsuario).subscribe(res => {
      console.log(res);
    },
    error => {
      console.log('ERROR', error);
    });
  }
}
