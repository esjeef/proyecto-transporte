export interface UpdateClave {
  identificador: number;
  claveAntigua: string;
  claveNueva: string;
}
