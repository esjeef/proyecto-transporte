export interface TarjetaPago {
  identificadorCliente: number;
  numeroTarjetaTransporte: string;
  montoRecarga: number;
  numeroTarjetaPago: string;
  fechaExpiracion: string;
  CCV: string;
  nombreTitular: string;
  correoElectronico: string;
}
