export interface User {
  identificador: string;
  usuario: string;
  correo: string;
  dni: string;
  numeroCelular: string;
  clave: string;
}
