export interface Tarjeta {
  listaTarjetas: any;
  idTarjeta: string;
  numeroTarjeta: string;
  tipoServicio: string;
  estadoTarjeta: string;
  saldo: string;
  fechaVencimiento: string;
}
