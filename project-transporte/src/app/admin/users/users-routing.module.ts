import { ActualizarPasswordComponent } from './containers/actualizar-password/actualizar-password.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersListComponent } from './containers/users-list/users-list.component';
import { ListarTarjetaComponent } from './containers/listar-tarjeta/listar-tarjeta.component';
import { AsociarTarjetaComponent } from './containers/asociar-tarjeta/asociar-tarjeta.component';
import { RecargarTarjetaComponent } from './containers/recargar-tarjeta/recargar-tarjeta.component';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

const routes: Routes = [
  { path: '', component: ListarTarjetaComponent },
  { path: 'listarTarjeta', component: ListarTarjetaComponent },
  { path: 'asociarTarjeta', component: AsociarTarjetaComponent },
  { path: 'recargarTarjeta', component: RecargarTarjetaComponent },
  { path: 'recargarTarjeta/:tarjeta', component: RecargarTarjetaComponent },
  { path: 'actualizarPassword', component: ActualizarPasswordComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
