import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../admin.service';

@Component({
  selector: 'app-asociar-tarjeta',
  templateUrl: './asociar-tarjeta.component.html',
  styleUrls: ['../../../../../assets/css/style.css']
})
export class AsociarTarjetaComponent implements OnInit {
  public asociarTarjetas = { identificador: '' , asociarTarjetas : [{ numeroTarjeta: '', tipoServicio: 0 , FechaVencimiento : ''}]};
  public imgTarjetaM = false;
  public imgTarjetaL = false;
  public imgTarjetaNull = true;
  tipoTarjetas = [];

  constructor(
    public admService: AdminService
  ) { }

  ngOnInit() {
    this.tipoTarjetas = [
      { id: 0, description: '--Seleccione Tarjeta--' },
      { id: 1, description: 'Tarjeta Metropolitano' },
      { id: 2, description: 'Tarjeta Linea 1' }
    ];
  }

  asociarTarjeta(){
    this.asociarTarjetas.identificador = localStorage.getItem('identificador');
    this.asociarTarjetas.asociarTarjetas[0].FechaVencimiento = '22/04/2020';
    this.admService.asociarTarjeta(this.asociarTarjetas).subscribe(res => {
      console.log(res);
    });
  }

  tarjetaSelect(evento){
    switch (evento.target.value) {
      case '1':
        this.imgTarjetaL = false;
        this.imgTarjetaM = true;
        this.imgTarjetaNull = false;
        break;
      case '2':
        this.imgTarjetaL = true;
        this.imgTarjetaM = false;
        this.imgTarjetaNull = false;
        break;
      default:
        this.imgTarjetaL = false;
        this.imgTarjetaM = false;
        this.imgTarjetaNull = true;
        break;
    }
  }

}
