import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminService } from '../../admin.service';
@Component({
  selector: 'app-recargar-tarjeta',
  templateUrl: './recargar-tarjeta.component.html',
  styleUrls: ['../../../../../assets/css/style.css']
})
export class RecargarTarjetaComponent implements OnInit {
  public tarjetaCredito = {
    identificadorCliente: 0,
    numeroTarjetaTransporte: '',
    montoRecarga: 0 ,
    numeroTarjetaPago : '',
    fechaExpiracion: '',
    CCV: '',
    nombreTitular: ' ',
    correoElectronico: ''
  };
  tarjeta: string;
  public tarjetaTransporte = false;

  constructor(private actRoute: ActivatedRoute, public admService: AdminService) {
    this.tarjeta = this.actRoute.snapshot.paramMap.get('tarjeta');
  }

  ngOnInit() {
    if (this.tarjeta == null){
      this.tarjetaTransporte = true;
    }else{
      this.tarjetaTransporte = false;
    }
  }

  realizarPago(){
    // tslint:disable-next-line: radix
    this.tarjetaCredito.identificadorCliente = parseInt(localStorage.getItem('identificador'));
    this.tarjetaCredito.numeroTarjetaTransporte = this.tarjeta;
    this.admService.realizarPago(this.tarjetaCredito).subscribe(res => {
      console.log(res);
    });
  }

}
