import { Tarjeta } from './../../../../core/models/tarjeta';
import { AdminService } from './../../admin.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listar-tarjeta',
  templateUrl: './listar-tarjeta.component.html',
  styleUrls: ['../../../../../assets/css/style.css']
})
export class ListarTarjetaComponent implements OnInit {
  public tarjetas: any = [];
  public tarjetaCount: number;
  public tarjetaAsociadas = [];
  public spinner = true;
  public viewlist = false;
  constructor(
    public admService: AdminService
  ) { }

  ngOnInit() {
    this.tarjetaAsociadas = [
      { identificador: localStorage.getItem('identificador')}
    ];
    this.listarTarjeta();
  }

  listarTarjeta(){
    this.admService.listarTarjeta(this.tarjetaAsociadas[0].identificador).subscribe(res => {
      this.tarjetas = res.listaTarjetas;
      if (this.tarjetas != null){
        this.tarjetaCount = this.tarjetas.length;
      }else{
        this.tarjetaCount = 0;
      }
      this.spinner = false;
      this.viewlist = true;
    });
  }
}
