import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../admin.service';

@Component({
  selector: 'app-actualizar-password',
  templateUrl: './actualizar-password.component.html',
  styleUrls: ['../../../../../assets/css/style.css']
})
export class ActualizarPasswordComponent implements OnInit {
  public updateClave = {
    identificador: '',
    claveAntigua: '',
    claveNueva: ''
  };
  confirmarPassword: string;
  constructor(public admService: AdminService) { }

  ngOnInit() {
    this.confirmarPassword = '';
  }

  updatePassword(){
    this.updateClave.identificador = localStorage.getItem('identificador');
    this.admService.updateClave(this.updateClave).subscribe(res => {
      console.log(res);
    },
    error => {
      console.log('ERROR', error);
    });
  }

}
