import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Tarjeta } from 'src/app/core/models/tarjeta';
import { TarjetaPago } from 'src/app/core/models/tarjetaPago';
import { UpdateClave } from 'src/app/core/models/updateClave';

@Injectable({
  providedIn: 'root'
})

export class AdminService {

  baseurl = 'http://71d12874.ngrok.io/transporte/publico/';

  constructor(private http: HttpClient) { }

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  listarTarjeta(id): Observable<Tarjeta> {
    return this.http.get<Tarjeta>(this.baseurl + 'listarTarjeta/' + id)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    );
  }

  asociarTarjeta(data): Observable<Tarjeta> {
    return this.http.post<Tarjeta>(this.baseurl + 'asociarTarjeta', JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    );
  }

  realizarPago(data): Observable<TarjetaPago> {
    return this.http.post<TarjetaPago>(this.baseurl + 'recargarTarjeta', JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    );
  }

  updateClave(data): Observable<UpdateClave> {
    return this.http.post<UpdateClave>(this.baseurl + 'actualizarClave', JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    );
  }

  // Error handling
  errorHandl(error) {
     let errorMessage = '';
     if (error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     console.log(errorMessage);
     return throwError(errorMessage);
  }

}
