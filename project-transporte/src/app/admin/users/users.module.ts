import { SpinnerLoadComponent } from './containers/spinner-load/spinner-load.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../core/shared/shared.module';
import { UsersRoutingModule } from './users-routing.module';
import { UsersListComponent } from './containers/users-list/users-list.component';
import { CommonModule } from '@angular/common';
import { AsociarTarjetaComponent } from './containers/asociar-tarjeta/asociar-tarjeta.component';
import { ListarTarjetaComponent } from './containers/listar-tarjeta/listar-tarjeta.component';
import { RecargarTarjetaComponent } from './containers/recargar-tarjeta/recargar-tarjeta.component';
import { AdminService } from './admin.service';
import { ActualizarPasswordComponent } from './containers/actualizar-password/actualizar-password.component';

@NgModule({
  imports: [
    SharedModule,
    UsersRoutingModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
  ],
  declarations: [
    ActualizarPasswordComponent,
    UsersListComponent,
    AsociarTarjetaComponent,
    ListarTarjetaComponent,
    RecargarTarjetaComponent,
    SpinnerLoadComponent
  ],
  exports: [],
  providers: [AdminService]
})

export class UsersModule {
  constructor() {}
}
